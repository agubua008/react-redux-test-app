# React Redux Frontend Test App

Bienvenidos a la prueba de frontent de AXA.
Es proyecto tiene como objetivo demostrar las habilidades de programacion en mediante ReactJs y Redux.
Para la realizacion de este ejemplo me basé en un template descargado de internet para
dedicar el tiempo en la parte de React y Redux. (Igualmente tuve que realizar muchisimas modificaciones para llegar a la estrutura deseada)

Template Usado:
    [Template](https://www.creative-tim.com/new-issue/paper-dashboard-react)


Requisitos previos (Sin Docker):
    
*  Debe tener instalado NodeJs en la PC. [NodeJS](https://nodejs.org/es/)

Pasos para ejecutar el proyecto:
*  Ejecular el comando `git clone https://gitlab.com/agubua008/react-redux-test-app.git`
*  Ejecutar el comando `cd react-redux-test-app`
*  Ejecutar el comando `npm i` y esperar que se instalen todas las dependencias
*  Ejecutar el comando `npm start` y esperar que el browser por defecto de ejecute o ingresar a su navegador favorito e ingresar a [http://localhost:3000](http://localhost:3000)


Requisitos previos (Con Docker):
    
*  Debe tener instalado Docker en la PC. [Docker](https://docs.docker.com/install/)

Pasos para ejecutar el proyecto:
*  Ejecular el comando `docker run --rm -p 3000:3000 node /bin/bash -c "git clone https://gitlab.com/agubua008/react-redux-test-app.git && cd react-redux-test-app && npm install && npm start"`



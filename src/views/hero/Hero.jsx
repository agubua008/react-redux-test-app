import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Row,
  Col
} from "reactstrap";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class HeroComponent extends React.Component {

  renderHeroFriends(hero) {
    if (!hero || !hero.friends || hero.friends.length === 0) {
      return <></>;
    }
    const friends = this.props.completeList.filter(p => {
      return hero.friends.indexOf(p.name) !== -1;
    });

    return friends.map((friend, idx) => (
      <li key={idx}>
        <Row>
          <Col md="2" xs="2">
            <div className="avatar">
              <img
                alt="..."
                className="img-circle img-no-padding img-responsive"
                src={friend.thumbnail}
              />
            </div>
          </Col>
          <Col md="7" xs="7">
            {friend.name}
          </Col>
          <Col className="text-right" md="3" xs="3">
          </Col>
        </Row>
      </li>
    ))
  }

  renderHeroProfessions(hero) {
    if (!hero || !hero.professions || hero.professions.length === 0) {
      return <p className="card-text d-inline-block mb-3 text-muted">
        <b>Professions: </b>Does not have</p>;
    }
    return hero.professions.map((prof, idx) => (
      <li key={idx}>
        <Row>
          <Col md="12" xs="12">
            {prof}
          </Col>
        </Row>
      </li>
    ));
  }

  render() {
    if (!this.props.match || !this.props.match.params.id || isNaN(parseInt(this.props.match.params.id)) ||
      !this.props || !this.props.completeList || this.props.completeList.length === 0) {
      this.props.history.goBack();
      return null;
    }
    const id = parseInt(this.props.match.params.id);
    const hero = this.props.completeList.find(p => p.id === id);
    return (
      <>
        <div className="content">
          <Row>
            <Col md="2"></Col>
            <Col md="4">
              <Card className="card-user">
                <div className="image">
                  <img
                    alt="..."
                    src={hero.thumbnail}
                  />
                </div>
                <CardBody>
                  <div className="author">

                    <img
                      alt="..."
                      className="avatar border-gray"
                      src={hero.thumbnail}
                    />
                    <h5 className="title">{hero.name}</h5>
                    <p className="description"><b>Hair Color </b>{hero.hair_color}</p>
                  </div>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="button-container">
                    <Row>
                      <Col className="ml-auto" lg="3" md="6" xs="6">
                        <h5>
                          {Math.floor(hero.height)} <br />
                          <small>Height</small>
                        </h5>
                      </Col>
                      <Col className="ml-auto mr-auto" lg="4" md="6" xs="6">
                        <h5>
                          {hero.age} <br />
                          <small>Age</small>
                        </h5>
                      </Col>
                      <Col className="mr-auto" lg="3">
                        <h5>
                          {Math.floor(hero.weight)} <br />
                          <small>Weight</small>
                        </h5>
                      </Col>
                    </Row>
                  </div>
                </CardFooter>
              </Card>
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Friends</CardTitle>
                </CardHeader>
                <CardBody>
                  <ul className="list-unstyled team-members">
                    {this.renderHeroFriends(hero)}
                  </ul>
                </CardBody>
              </Card>
            </Col>
            <Col md="4">
              <Card className="card-user">
                <CardHeader>
                  <CardTitle tag="h4">Professions</CardTitle>
                </CardHeader>
                <CardBody>
                  <ul className="list-unstyled team-members">
                    {this.renderHeroProfessions(hero)}
                  </ul>
                </CardBody>
              </Card>
            </Col>
            <Col md="2"></Col>
          </Row>
        </div>
      </>
    );
  }
}

HeroComponent.propTypes = {
  match: PropTypes.any.isRequired,
  history: PropTypes.any.isRequired,
  visibleList: PropTypes.any.isRequired,
  completeList: PropTypes.any.isRequired
};


const mapStateToProps = (state) => {
  const { herosData } = state.heros;
  return {
    completeList: herosData
  }
};

export default connect(
  mapStateToProps,
  null
)(HeroComponent);



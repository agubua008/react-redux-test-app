import * as React from 'react';
import {
    Row, Col,
    Card, CardBody,
    Badge, Input, InputGroup,
    InputGroupAddon, InputGroupText
} from "reactstrap";
import {
    CellMeasurer,
    CellMeasurerCache,
    createMasonryCellPositioner,
    Masonry,
    AutoSizer
} from 'react-virtualized';
import { Link } from "react-router-dom";

import { connect } from 'react-redux';
import { herosOperations } from '../../duck';
import PropTypes from 'prop-types';


class CensusBoardComponent extends React.Component {

    constructor(props, context) {
        super(props, context);

        this._columnCount = 0;
        this._width = 0;
        this._cache = new CellMeasurerCache({
            defaultHeight: 260,
            defaultWidth: 299,
            fixedWidth: true,
            minHeight: 320
        });
        this._masonry = React.createRef();
        this._cellPositioner = undefined;

        this.state = {
            columnWidth: 295,
            height: 800,
            gutterSize: 10,
        };

        this.renderHeroCards = this.renderHeroCards.bind(this);
        this.renderAutoSizer = this.renderAutoSizer.bind(this);
        this.renderHeroCardsMasonry = this.renderHeroCardsMasonry.bind(this);

        this.onAutoSizerResize = this.onAutoSizerResize.bind(this);
        this.setHeroCardsMasonryRef = this.setHeroCardsMasonryRef.bind(this);
    }

    componentDidMount() {
        setTimeout(() => {
            this.props.fetchHerosJson();
        }, 100);
    }

    onInputChange(event) {
        const name = event.target.value.toLowerCase();
        this.props.filterHerosJson(name);
    }

    render() {
        const {
            height
        } = this.state;
        const child = this.renderAutoSizer({ height });
        return (
            <div className="content">
                <Row>
                    <Col lg="12" md="12" sm="12" >
                        <InputGroup className="no-border">
                            <Input placeholder="Search..." onKeyUp={this.onInputChange.bind(this)} />
                            <InputGroupAddon addonType="append">
                                <InputGroupText>
                                    <i className="nc-icon nc-zoom-split" />
                                </InputGroupText>
                            </InputGroupAddon>
                        </InputGroup>
                    </Col>
                </Row>
                <Row>
                    <Col lg="12" md="12" sm="12" >
                        {child}
                    </Col>
                </Row>
            </div>
        );
    }

    renderHeroCards({ index, key, parent, style }) {
        const { visibleList } = this.props;
        const hero = visibleList[index];
        if (hero === undefined) {
            return;
        }
        return (
            <CellMeasurer cache={this._cache} index={index} key={key} parent={parent}>
                <Card style={style} className="card-post card-post--1">
                    <div className="card-post__image"
                        style={{ backgroundImage: `url(${hero.thumbnail})` }}>
                        <Badge pill className={`card-post__category bg-${hero.categoryTheme}`}>
                            {hero.category}
                        </Badge>
                        <div className="card-post__author d-flex">
                            {this.renderHeroFriends(hero)}
                        </div>
                    </div>
                    <CardBody>
                        <h5 className="card-title">
                            <Link to={`/admin/hero/${hero.id}`} className="text-fiord-blue">{hero.name}</Link>
                        </h5>
                        {this.renderHeroProfessions(hero)}
                        <br />
                        <span className="text-muted"><b>Age:</b> {hero.age}</span>
                        <span className="text-muted"><b> Height</b> {Math.floor(hero.height)}</span>
                        <span className="text-muted"><b> Weight:</b> {Math.floor(hero.weight)}</span>
                    </CardBody>
                </Card>
            </CellMeasurer>
        );
    }

    renderHeroFriends(hero) {
        if (!hero || !hero.friends || hero.friends.length === 0) {
            return <></>;
        }
        const friends = this.props.completeList.filter(p => {
            return hero.friends.indexOf(p.name) !== -1;
        });
        return friends.map((friend) => (
            <Link to={`/admin/hero/${friend.id}`} title={friend.name} key={`hero/${friend.id}`}
                className="card-post__author-avatar card-post__author-avatar--small"
                style={{ backgroundImage: `url('${friend.thumbnail}')` }}>
            </Link>
        ))
    }

    renderHeroProfessions(hero) {
        if (!hero || !hero.professions || hero.professions.length === 0) {
            return <p className="card-text d-inline-block mb-3 text-muted">
                <b>Professions: </b>Does not have</p>;
        }
        return <p className="card-text d-inline-block mb-3 text-muted">
            <b>Professions: </b>
            {hero.professions.map((prof, idx) => (
                <span key={`${idx}`} >{prof}{(idx + 1) < hero.professions.length ? ', ' : ''} </span>
            ))}
        </p>;
    }

    initHeroCardsPositioner() {
        if (this._cellPositioner === undefined) {
            const { columnWidth, gutterSize } = this.state;
            this._cellPositioner = createMasonryCellPositioner({
                cellMeasurerCache: this._cache,
                columnCount: this._columnCount,
                columnWidth,
                spacer: gutterSize,
            });
        }
    }

    calculateHeroCardsColumnCount() {
        const { columnWidth, gutterSize } = this.state;
        this._columnCount = Math.floor(this._width / (columnWidth + gutterSize));
    }

    onAutoSizerResize({ width }) {
        this._width = width;

        this.calculateHeroCardsColumnCount();
        this.resetCellPositioner();
        this._masonry.recomputeCellPositions();
    }

    resetCellPositioner() {
        const { columnWidth, gutterSize } = this.state;
        this._cellPositioner.reset({
            columnCount: this._columnCount,
            columnWidth,
            spacer: gutterSize,
        });
    }

    renderAutoSizer({ height, scrollTop }) {
        this._height = height;
        this._scrollTop = scrollTop;
        const { overscanByPixels } = this.state;
        const { visibleList } = this.props;
        return (
            <AutoSizer
                disableHeight
                height={height}
                rowCount={visibleList.length}
                onResize={this.onAutoSizerResize}
                overscanByPixels={overscanByPixels}
                scrollTop={this._scrollTop}>
                {this.renderHeroCardsMasonry}
            </AutoSizer>
        );
    }

    renderHeroCardsMasonry({ width }) {
        this._width = width;
        this.calculateHeroCardsColumnCount();
        this.initHeroCardsPositioner();

        const { height, overscanByPixels, windowScrollerEnabled } = this.state;
        const { visibleList } = this.props;
        return (
            <Masonry
                cellCount={visibleList.length}
                cellMeasurerCache={this._cache}
                cellPositioner={this._cellPositioner}
                cellRenderer={this.renderHeroCards}
                height={windowScrollerEnabled ? this._height : height}

                overscanByPixels={overscanByPixels}
                ref={this.setHeroCardsMasonryRef}
                scrollTop={this._scrollTop}
                width={width}
            />
        );
    }

    setHeroCardsMasonryRef(ref) {
        this._masonry = ref;
    }
}


CensusBoardComponent.propTypes = {
    visibleList: PropTypes.any.isRequired,
    completeList: PropTypes.any.isRequired,
    fetchHerosJson: PropTypes.func.isRequired,
    filterHerosJson: PropTypes.func.isRequired
  };

const mapStateToProps = (state) => {
    const { herosData, filteredHerosData, showSpinner } = state.heros;
    return {
        visibleList: filteredHerosData,
        completeList: herosData,
        showSpinner: showSpinner
    }
};

const mapDispatchToProps = (dispatch) => {
    const fetchHerosJson = () => {
        dispatch(herosOperations.fetchHerosJson())
    };

    const filterHerosJson = (name) => {
        dispatch(herosOperations.filterHerosJson(name))
    };

    return { fetchHerosJson, filterHerosJson };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CensusBoardComponent);


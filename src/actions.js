export const UPDATE_HEROS = 'UPDATE_HEROS'

export function addHeros(heros) {
    return { type: UPDATE_HEROS, heros }
}

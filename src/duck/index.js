import herosReducer from './reducers';
export { default as herosOperations } from './operations';
export { default as herosTypes } from './types';
export default herosReducer;
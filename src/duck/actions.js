import { createActions } from 'reduxsauce';

const { Creators, Types } = createActions({
  requestHerosJson: ['heros'],
  receiveHerosJson: ['herosData'],
  filterHerosJson: ['filterHerosData']
});

export { Creators, Types };
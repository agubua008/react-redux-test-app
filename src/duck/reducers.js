import types from './types';

const INITIAL_STATE = {
  count: 0,
  error: null,
  showSpinner: false,
  herosData: [],
  filteredHerosData: []
}
const herosReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {

    case types.REQUEST_HEROS_JSON: {
      return {
        ...state,
        count: 0,
        error: null,
        herosData: [],
        filteredHerosData: [],
        showSpinner: true
      }
    }

    case types.RECEIVE_HEROS_JSON: {
      const { herosData, error } = action;
      return {
        ...state,
        count: herosData.length,
        error,
        herosData,
        filteredHerosData: herosData,
        showSpinner: false
      }
    }

    case types.FILTER_HEROS_JSON: {
      const { filterHerosData } = action;

      const data = state.herosData.filter(p => p.name.toLowerCase().includes(filterHerosData));

      return {
        ...state,
        filteredHerosData: data
      }
    }

    default: return state;
  }
}

export default herosReducer;
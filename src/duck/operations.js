import { Creators } from './actions';

const requestHerosJsonAction = Creators.requestHerosJson;
const receiveHerosJsonAction = Creators.receiveHerosJson;
const filterHerosJsonAction = Creators.filterHerosJson;

const fetchHerosJson = () => {
  return dispatch => {
    dispatch(requestHerosJsonAction());
    return fetch(`https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json`)
      .then(response => response.json())
      .then(json => {
        const data = json.Brastlewark;
        dispatch(receiveHerosJsonAction(data))
      }).catch(error => {
        console.log(error);
        receiveHerosJsonAction({ herosData: [], error: error });
      });
  }
};

const filterHerosJson = (name) => {
  return dispatch => {
    dispatch(filterHerosJsonAction(name))
  }
};

export default {
  fetchHerosJson,
  filterHerosJson
}
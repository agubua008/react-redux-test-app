import herosReducer from './index.js';
import types from './types.js';
import { Creators } from './actions';

describe("Test herosReducer", () => {
    test("Test if herosReducer filter correctly heros by name", () => {

        const initialState = {
            count: 0,
            error: null,
            showSpinner: false,
            herosData: [{
                "id": 0,
                "name": "Tobus Chromerocket",
                "thumbnail": "http://www.publicdomainpictures.net/pictures/10000/nahled/thinking-monkey-11282237747K8xB.jpg",
                "age": 306,
                "weight": 39.065952,
                "height": 107.75835,
                "hair_color": "Pink",
                "professions": [
                    "Metalworker",
                    "Woodcarver",
                    "Stonecarver",
                    " Tinker",
                    "Tailor",
                    "Potter"
                ],
                "friends": [
                    "Cogwitz Chillwidget",
                    "Tinadette Chillbuster"
                ]
            },
            {
                "id": 1,
                "name": "Tinadette Chillbuster",
                "thumbnail": "http://www.publicdomainpictures.net/pictures/120000/nahled/white-hen.jpg",
                "age": 288,
                "weight": 35.279167,
                "height": 110.43628,
                "hair_color": "Green",
                "professions": [
                    "Brewer",
                    "Medic",
                    "Prospector",
                    "Gemcutter",
                    "Mason",
                    "Tailor"
                ],
                "friends": []
            },
            {
                "id": 2,
                "name": "Cogwitz Chillwidget",
                "thumbnail": "http://www.publicdomainpictures.net/pictures/30000/nahled/maple-leaves-background.jpg",
                "age": 166,
                "weight": 35.88665,
                "height": 106.14395,
                "hair_color": "Red",
                "professions": [
                    "Cook",
                    "Baker",
                    "Miner"
                ],
                "friends": [
                    "Fizwood Voidtossle"
                ]
            }],
            filteredHerosData: []
        };

        const result = herosReducer(initialState, { type: types.FILTER_HEROS_JSON, filterHerosData: "chill" });
        expect(result).toBeTruthy();
        expect(result.filteredHerosData).toBeTruthy();
        expect(result.filteredHerosData.length).toBe(2);

    });
});

describe('Test actions', () => {
    it('Should create an action type equals to "FILTER_HEROS_JSON" with a filterHerosData equals to "chill"', () => {
        const expectedAction = {
            type: types.FILTER_HEROS_JSON,
            filterHerosData: "chill"
        };

        const filterHerosJsonAction = Creators.filterHerosJson("chill");
        expect(filterHerosJsonAction).toEqual(expectedAction)
    });
});
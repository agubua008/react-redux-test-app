import React from "react";
import {
  Navbar,
  NavbarBrand,
  Container
} from "reactstrap";
import routes from "routes.js";

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.sidebarToggle = React.createRef();
  }
  getTitle() {
    let title = "Default Title";
    routes.map((prop) => {
      var url = (prop.layout + prop.path).replace("/:id", "");
      if (window.location.href.indexOf(url) !== -1) {
        title = prop.name;
      }
      return null;
    });
    return title;
  }
  openSidebar() {
    document.documentElement.classList.toggle("nav-open");
    this.sidebarToggle.current.classList.toggle("toggled");
  }
  componentDidUpdate(e) {
    if (
      window.innerWidth < 993 &&
      e.history.location.pathname !== e.location.pathname &&
      document.documentElement.className.indexOf("nav-open") !== -1
    ) {
      document.documentElement.classList.toggle("nav-open");
      this.sidebarToggle.current.classList.toggle("toggled");
    }
  }
  render() {
    return (
      <Navbar
        color={"dark"}
        expand="lg"
        className={"navbar-absolute fixed-top navbar-transparent"}
      >
        <Container fluid>
          <div className="navbar-wrapper">
            <div className="navbar-toggle">
              <button
                type="button"
                ref={this.sidebarToggle}
                className="navbar-toggler"
                onClick={() => this.openSidebar()}
              >
                <span className="navbar-toggler-bar bar1" />
                <span className="navbar-toggler-bar bar2" />
                <span className="navbar-toggler-bar bar3" />
              </button>
            </div>
            <NavbarBrand>{this.getTitle()}</NavbarBrand>
          </div>
        </Container>
      </Navbar>
    );
  }
}

export default Header;

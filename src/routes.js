import HeroComponent from "views/hero/Hero.jsx";
import CensusBoardComponent from "views/censusBoard/CensusBoard.jsx";

var routes = [
  {
    path: "/census",
    name: "Census Board",
    icon: "nc-icon nc-world-2",
    component: CensusBoardComponent,
    layout: "/admin",
    show: true,
  },
  {
    path: "/hero/:id",
    name: "Hero Profile",
    icon: "nc-icon nc-single-02",
    component: HeroComponent,
    layout: "/admin",
    show: false,
  }
];
export default routes;

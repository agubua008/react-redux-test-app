import React from "react";
import { Provider } from 'react-redux'
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.css";
import "assets/scss/paper-dashboard.scss?v=1.1.0";
import "assets/demo/demo.css";
import "perfect-scrollbar/css/perfect-scrollbar.css";

import Template from "layouts/Template.jsx";
import thunk from 'redux-thunk';
import {
  createStore,
  applyMiddleware
} from 'redux';
import rootReducer from './reducers';
import * as serviceWorker from './serviceWorker';

const middleware = applyMiddleware(thunk);
const store = createStore(rootReducer, middleware);
const hist = createBrowserHistory();

ReactDOM.render(
  <Provider store={store}>
    <Router history={hist}>
      <Switch>
        <Route path="/admin" render={props => <Template {...props} />} />
        <Redirect to="/admin/census" />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById("root")
);
serviceWorker.unregister();
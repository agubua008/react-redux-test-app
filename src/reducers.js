import { combineReducers } from 'redux';
import herosReducer from './duck/reducers';

const rootReducer = combineReducers({
    heros: herosReducer
});

export default rootReducer;